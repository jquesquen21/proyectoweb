package com.bdd.step;

import com.bdd.page.LoginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class CompraStep extends ScenarioSteps {

    LoginPage loginPage;

    @Step
    public void que_abro_la_pagina_sauceDemo_desde_un_navegador() {
        loginPage.que_abro_la_pagina_sauceDemo_desde_un_navegador();
    }

    @Step
    public void ingreso_el_usuario_y_contrasena(String usuario, String password) {
        loginPage.ingreso_el_usuario_y_contrasena(usuario,password);
    }

    @Step
    public void el_usuario_agregado_productos_al_carrito(int cantidad) {
        loginPage.el_usuario_agregado_productos_al_carrito(cantidad);
    }

    @Step
    public void el_usuario_visualiza_el_carrito() {
        loginPage.el_usuario_visualiza_el_carrito();
    }

    @Step
    public void el_usuario_completa_el_formulario_de_compra(String nombre, String apellido, String codigoPostal) {
        loginPage.el_usuario_completa_el_formulario_de_compra(nombre,apellido,codigoPostal);
    }

    @Step
    public void valido_que_la_compra_se_finaliza_con_exito_y_se_muestra_el_mensaje(String mensaje) {
        loginPage.valido_que_la_compra_se_finaliza_con_exito_y_se_muestra_el_mensaje(mensaje);
    }

}

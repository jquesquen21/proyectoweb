package com.bdd.page;

import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.ScrollToWebElement;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


@DefaultUrl("https://www.saucedemo.com/")
public class LoginPage extends PageObject {

    @FindBy(xpath = "//input[@id='user-name']")
    private WebElement userName;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement clave;

    @FindBy(xpath = "//input[@value='Login']")
    private WebElement btnLogin;

    @FindBy(xpath = "//button[text()='Checkout']")
    private WebElement checkout;

    @FindBy(id = "first-name")
    private WebElement firstNameField;

    @FindBy(id = "last-name")
    private WebElement lastNameField;

    @FindBy(id = "postal-code")
    private WebElement postaCodeField;

    @FindBy(xpath = "//input[@value='Continue']")
    private WebElement continueButton;

    @FindBy(xpath = "//a[text()='Finish']")
    private WebElement finishButton;

    @FindBy(xpath = "//div[@id=\"checkout_complete_container\"]/h2")
    private WebElement confirmationMenssage;

    public void que_abro_la_pagina_sauceDemo_desde_un_navegador() {
        String navegador = "chrome";
        Browser.Start(this,navegador);
    }

    public void ingreso_el_usuario_y_contrasena(String usuario, String password){

        userName.sendKeys(usuario);
        clave.sendKeys(password);
        btnLogin.click();
    }

    public void el_usuario_agregado_productos_al_carrito(int cantidad){

        for(int i = 1; i <= cantidad; i++){
            WebElement addToCartButton = getDriver().findElement(By.xpath("(//button[text()='Add to cart'])[" + i + "]"));
            addToCartButton.click();
        }
    }

    public void el_usuario_visualiza_el_carrito(){
        WebElement cartButton = getDriver().findElement(By.className("shopping_cart_link"));
        cartButton.click();
        checkout.click();
    }

    public void el_usuario_completa_el_formulario_de_compra(String nombre, String apellido, String codigoPostal){

        firstNameField.sendKeys(nombre);
        lastNameField.sendKeys(apellido);
        postaCodeField.sendKeys(codigoPostal);
        continueButton.click();

        WebElement elemento = getDriver().findElement(By.xpath("//button[text()='Finish']"));
//        new Scroll(boton);
        elemento.click();


    }
    public void valido_que_la_compra_se_finaliza_con_exito_y_se_muestra_el_mensaje(String mensaje){

        String texto = confirmationMenssage.getText();
        System.out.println(texto);
        //VALIDACIÓN
        if (texto.equals(mensaje)) {
            Assert.assertTrue(true);
        } else {
            Assert.fail("No se encontro el menaje");
        }
        //CIERRA EL NAVEGADOR
        getDriver().close();

    }

}

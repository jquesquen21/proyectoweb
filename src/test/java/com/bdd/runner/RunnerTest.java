package com.bdd.runner;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import net.thucydides.core.webdriver.DriverSource;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(plugin = {"html:target/build/cucumber-html-report", "pretty:target/build/cucumber-pretty.txt",
            "json:target/build/cucumber.json"},
            junit = {"--step-notifications"},
            features = {"src/test/resources/web"},
            glue = {"com.bdd"},
            tags = {"@ESCENARIO_COMPRA"}
)

public class RunnerTest implements DriverSource {
        @Override
        public WebDriver newDriver() {
            System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
            ChromeOptions options = new ChromeOptions();
            return new ChromeDriver(options);
        }
        @Override public boolean takesScreenshots() {
            return true;
        }

}

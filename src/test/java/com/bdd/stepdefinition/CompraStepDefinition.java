package com.bdd.stepdefinition;

import com.bdd.step.CompraStep;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;
import net.thucydides.core.annotations.Steps;

public class CompraStepDefinition {

    @Steps
    CompraStep compraStep;

    @Dado("^que abro la pagina SauceDemo desde un navegador$")
    public void que_abro_la_pagina_sauceDemo_desde_un_navegador(){
        compraStep.que_abro_la_pagina_sauceDemo_desde_un_navegador();
    }

    @Cuando("^ingreso el usuario \"([^\"]*)\" y contrasena \"([^\"]*)\"$")
    public void ingreso_el_usuario_y_contrasena(String usuario, String password)throws Throwable  {
        compraStep.ingreso_el_usuario_y_contrasena(usuario,password);
    }

    @Y("^el usuario agregado \"([^\"]*)\" productos al carrito$")
    public void el_usuario_agregado_productos_al_carrito(int cantidad) throws Throwable {
        compraStep.el_usuario_agregado_productos_al_carrito(cantidad);
    }

    @Y("^el usuario visualiza el carrito$")
    public void el_usuario_visualiza_el_carrito(){
        compraStep.el_usuario_visualiza_el_carrito();
    }

    @Y("^el usuario completa el formulario de compra  \"([^\"]*)\", \"([^\"]*)\" y \"([^\"]*)\"$")
    public void el_usuario_completa_el_formulario_de_compra(String nombre, String apellido, String codigoPostal)throws Throwable {
        compraStep.el_usuario_completa_el_formulario_de_compra(nombre, apellido,codigoPostal);
    }

    @Entonces("^valido que la compra se finaliza con exito y se muestra el mensaje \"([^\"]*)\"$")
    public void valido_que_la_compra_se_finaliza_con_exito_y_se_muestra_el_mensaje(String mensaje)throws Throwable  {
        compraStep.valido_que_la_compra_se_finaliza_con_exito_y_se_muestra_el_mensaje(mensaje);
    }

}

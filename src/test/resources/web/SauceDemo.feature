#language: es
@FEATURE_DEMO
Característica: Compras de productos en SauceDemo

  Antecedentes: descripcion antecedente
  Como usuario xxxxxxx

  @ESCENARIO_COMPRA
  Esquema del escenario: Comprar poruductos
    Dado que abro la pagina SauceDemo desde un navegador
    Cuando ingreso el usuario "<usuario>" y contrasena "<password>"
    Y el usuario agregado "<cantidad>" productos al carrito
    Y el usuario visualiza el carrito
    Y el usuario completa el formulario de compra  "<nombre>", "<apellido>" y "<codigo>"
    Entonces valido que la compra se finaliza con exito y se muestra el mensaje "Thank you for your order!"

    Ejemplos:
      | usuario       | password     | nombre | apellido | codigo | cantidad |
      | standard_user | secret_sauce | Jose   | Quesquen | 740061 | 2        |



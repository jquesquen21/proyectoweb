# ProyectoWeb

## Getting started

Este proyecto contine feature de pruebas automatizadas utilizando el Serenity BDD Framework
para pruebas Web, debemos ingresar al GitLab y descargarlo.

## Prerequisites:

- Java JDK 8 o superior y SDK instalado
- Apache-maven 3.8.2
- IntelliJ IDEA

## Configuration and Execution:

- Clonar el proyecto desde el repositorio:

```
    - Ingrese a tu navegador preferido a la siguiente url:https://gitlab.com/jquesquen21/proyectoweb.git
    - Verifique estar posicionado en el repositorio del proyecto (main)
    - Da clic en el boton clone y copia su url
    - Create un archivo nuevo desde tu disco local c:
    - Desde tu explorador de archivo creado, abre el terminar Git 
    - Ejecute el siguiente comando: git clone https://gitlab.com/jquesquen21/proyectoweb.git
```
- Importar el proyecto clonado a IntelliJ IDEA

```
    - Abre IntelliJ IDEA.
    - Selecciona "File" en el menú principal y luego "Open".
    - Navega hasta el directorio del proyecto clonado y haz clic en "OK".
```

## Execute the Karate test cases

```
    - Espera a que IntelliJ IDEA importe y construya el proyecto.
    - Verifica que todas las dependencias se hayan descargado correctamente.
    - Navega hasta el paquete que contiene las pruebas automatizadas.
    - Ejecuta la clase "RunnerTest", desde la ruta: src/test/java/com.bdd/runner/RunnerTest.java
```

## Project Structure
```
    - src/test/resources/web/SauceDemo.feature: Es donde se encuentra definido nuestro caso automatizado.
    - src/test/java/com.bdd/stepdefinition: Class donde está vinculado paso a paso la definición del caso automatizado.
    - src/test/java/com.bdd/step: Class donde esta reclarada las instrucciones de pasos o acciones que se deben realizar en un escenario de prueba.
    - src/test/java/com.bdd/runner: Class donde se ejecuta los features creados. 
    - src/test/java/com.bdd/page: Class donde se encuentra la lógica de cada funcionalidad.
```



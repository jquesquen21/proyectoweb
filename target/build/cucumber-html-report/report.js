$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/web/SauceDemo.feature");
formatter.feature({
  "name": "Compras de productos en SauceDemo",
  "description": "",
  "keyword": "Característica",
  "tags": [
    {
      "name": "@FEATURE_DEMO"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Comprar poruductos",
  "description": "",
  "keyword": "Esquema del escenario",
  "tags": [
    {
      "name": "@ESCENARIO_COMPRA"
    }
  ]
});
formatter.step({
  "name": "que abro la pagina SauceDemo desde un navegador",
  "keyword": "Dado "
});
formatter.step({
  "name": "ingreso el usuario \"\u003cusuario\u003e\" y contrasena \"\u003cpassword\u003e\"",
  "keyword": "Cuando "
});
formatter.step({
  "name": "el usuario agregado \"\u003ccantidad\u003e\" productos al carrito",
  "keyword": "Y "
});
formatter.step({
  "name": "el usuario visualiza el carrito",
  "keyword": "Y "
});
formatter.step({
  "name": "el usuario completa el formulario de compra  \"\u003cnombre\u003e\", \"\u003capellido\u003e\" y \"\u003ccodigo\u003e\"",
  "keyword": "Y "
});
formatter.step({
  "name": "valido que la compra se finaliza con exito y se muestra el mensaje \"Thank you for your order!\"",
  "keyword": "Entonces "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Ejemplos",
  "rows": [
    {
      "cells": [
        "usuario",
        "password",
        "nombre",
        "apellido",
        "codigo",
        "cantidad"
      ]
    },
    {
      "cells": [
        "standard_user",
        "secret_sauce",
        "Jose",
        "Quesquen",
        "740061",
        "2"
      ]
    }
  ]
});
formatter.background({
  "name": "descripcion antecedente",
  "description": "  Como usuario xxxxxxx",
  "keyword": "Antecedentes"
});
formatter.scenario({
  "name": "Comprar poruductos",
  "description": "",
  "keyword": "Esquema del escenario",
  "tags": [
    {
      "name": "@FEATURE_DEMO"
    },
    {
      "name": "@ESCENARIO_COMPRA"
    }
  ]
});
formatter.step({
  "name": "que abro la pagina SauceDemo desde un navegador",
  "keyword": "Dado "
});
formatter.match({
  "location": "CompraStepDefinition.que_abro_la_pagina_sauceDemo_desde_un_navegador()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "ingreso el usuario \"standard_user\" y contrasena \"secret_sauce\"",
  "keyword": "Cuando "
});
formatter.match({
  "location": "CompraStepDefinition.ingreso_el_usuario_y_contrasena(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "el usuario agregado \"2\" productos al carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "CompraStepDefinition.el_usuario_agregado_productos_al_carrito(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "el usuario visualiza el carrito",
  "keyword": "Y "
});
formatter.match({
  "location": "CompraStepDefinition.el_usuario_visualiza_el_carrito()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "el usuario completa el formulario de compra  \"Jose\", \"Quesquen\" y \"740061\"",
  "keyword": "Y "
});
formatter.match({
  "location": "CompraStepDefinition.el_usuario_completa_el_formulario_de_compra(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "valido que la compra se finaliza con exito y se muestra el mensaje \"Thank you for your order!\"",
  "keyword": "Entonces "
});
formatter.match({
  "location": "CompraStepDefinition.valido_que_la_compra_se_finaliza_con_exito_y_se_muestra_el_mensaje(String)"
});
formatter.result({
  "status": "passed"
});
});